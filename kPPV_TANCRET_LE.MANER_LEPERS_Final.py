# coding: utf8

'''
Little-Project "Choixpeau magique"

Thiq program allows to associate a profile to the house of Hogwarts
corresponds according to its closest neighbours that depend on characters
of this character

Authors : Agathe Tancret, Noyale Le Maner, Aleksandr Lepers

Licence Creative Commons CC BY-ND
https://creativecommons.org/licenses/by-nd/4.0/

Version 4.1

11/03/2022
'''
# importation of folders
import csv
from math import sqrt

with open('Caracteristiques_des_persos.csv', mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characters_skills = [{key: value for key, value in element.items()} for
                         element in reader]

with open('Characters.csv', mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    characters = [{key: value for key, value in element.items()} for element
                  in reader]
characters_houses = []


# classe pour les couleurs affichées dans la console
class bcolors:
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    RED = '\033[91m'
    WHITE = '\033[0m'

# creation of a list with every characters' informations needed
for element in characters:
    info = {}
    info['Name'] = element['Name']
    info['House'] = element['House']
    characters_houses.append(info)

characters_infos = []

for skill in characters_skills:
    for house in characters_houses:
        if skill['Name'] == house['Name']:
            skill.update(house)
            characters_infos.append(skill)

# conversion of the skills' strings in integers
for people in characters_infos:
    people['Courage'] = int(people['Courage'])
    people['Ambition'] = int(people['Ambition'])
    people['Intelligence'] = int(people['Intelligence'])
    people['Good'] = int(people['Good'])


def distance(new_character, others_characters):
    '''
    ----------------------------------------------------------------------------
    Cette fonction permet de calculer la distance que le profil analysé a avec
    chaque personnage de la liste indiquée
    ----------------------------------------------------------------------------
    Entry -> new_character, List / others_characters, List of Dict
    Out -> others_characters, List of Dict
    ----------------------------------------------------------------------------
    '''
    gap = 0
    for character in others_characters:
        gap = sqrt((new_character['Courage'] - character['Courage']) ** 2 +
                   (new_character['Ambition'] - character['Ambition']) ** 2 +
                   (new_character['Intelligence'] -
                    character['Intelligence']) ** 2 +
                   (new_character['Good'] - character['Good']) ** 2)
        character['Gap'] = gap
    return others_characters


def close_neighbours(others_characters, k):
    '''
    ----------------------------------------------------------------------------
    Cette fonction permet de sélectionner les plus proches voisins en
    fonction de la distance calculée précédemment
    ----------------------------------------------------------------------------
    Entry -> others_characters, List of Dict / k, Int
    Out -> house_counter[0]['House'], Str / hogwarts_neighbours, List of Dict
    ----------------------------------------------------------------------------
    '''
    hogwarts_characters = []
    hogwarts_characters = sorted(others_characters, key=lambda x: x['Gap'],
                                 reverse=False)
    hogwarts_neighbours = []

    for neighbour in range(k):
        hogwarts_neighbours.append(hogwarts_characters[neighbour])
    return hogwarts_neighbours


def counter(hogwarts_neighbours):
    '''
    ----------------------------------------------------------------------------
    
    ----------------------------------------------------------------------------
    Entry -> 
    Out -> 
    ----------------------------------------------------------------------------
    '''
    house_counter = [{'House': 'Gryffindor', 'Counter': 0},
                     {'House': 'Slytherin', 'Counter': 0},
                     {'House': 'Hufflepuff', 'Counter': 0},
                     {'House': 'Ravenclaw', 'Counter': 0}]

    for house in hogwarts_neighbours:
        if house['House'] == 'Gryffindor':
            house_counter[0]['Counter'] += 1
        elif house['House'] == 'Slytherin':
            house_counter[1]['Counter'] += 1
        elif house['House'] == 'Hufflepuff':
            house_counter[2]['Counter'] += 1
        else:
            house_counter[3]['Counter'] += 1

    house_counter = sorted(house_counter, key=lambda x: x['Counter'],
                           reverse=True)
    
    neighbours = []

    for neighbour in hogwarts_neighbours:
        neighbours.append({neighbour['Name']: neighbour['House']})

    attributed_house = house_counter[0]['House']

    return attributed_house, neighbours



# definition of the steady
students = ({'Courage': 9, 'Ambition': 2, 'Intelligence': 8, 'Good': 9},
            {'Courage': 6, 'Ambition': 7, 'Intelligence': 9, 'Good': 7},
            {'Courage': 3, 'Ambition': 8, 'Intelligence': 6, 'Good': 3},
            {'Courage': 2, 'Ambition': 3, 'Intelligence': 7, 'Good': 8},
            {'Courage': 3, 'Ambition': 4, 'Intelligence': 8, 'Good': 8})
     

kppv = input(f"{bcolors.YELLOW}Would you like to choose the"
              " numbers of close neighbours or not ?"
             f"If you wish to, write"
             f" {bcolors.RED}Yes{bcolors.WHITE}.\n")
           
if kppv in ['Yes', 'yes']:
    k = 0
    while not 1 < k < 86:
        k = int(input(f"How many ? (It can not be over 86) : "))
else:
    k = 5

for student in students:
    house = counter(close_neighbours(distance(student, characters_infos), k))[0]
    neighbours = counter(close_neighbours(distance(student, characters_infos), k))[1]
    print(f"Votre maison est : {bcolors.RED}{house}{bcolors.WHITE}\nVos voisins sont {neighbours}\n")
    kppv = '0'